
# computes the bias of X by evaluating
compBias <- function(x, targetVal){
  sum(x - targetVal) / length(x)
}

#evaluateApproaches <- function(){
  # produce 100 estimates of mean and variance by 
  # importance sampler, self-normalized importance sampler as well as rejection samplier
  noEstimates <- 100
  
  # importanceSampler
  impS    <- sampleFromIS(noEstimates)
  # self-normalized importance sampler 
  snImpS  <- sampleFromIS(noEstimates, TRUE)
  #rejection sampler
  rejS    <- sampleFromRS(noEstimates)
  
  
  # create data frame suitable for boxplot
  frame <- data.frame(importance = impS[[1]], selfNormalized = snImpS[[1]], rejection = rejS[[1]])
  boxplot(frame, main='Distribution of Mu estimates')
  
  #frame <- data.frame(importance = impS[[2]], selfNormalized = snImpS[[2]], rejection = rejS[[2]])
  #boxplot(frame, main='Distribution of variance estimates')
  
  
  a <- 10
  b <- 3
  realMu <- a / (a + b)
  realVar <- (a * b) / ((a + b)^2 * (a + b + 1))
  
  impSBias <- c( compBias(impS[[1]], realMu), compBias(impS[[2]], realVar))
  snImpBias <- c( compBias(snImpS[[1]], realMu), compBias(snImpS[[2]], realVar))
  rejBias <- c( compBias(rejS[[1]], realMu), compBias(rejS[[2]], realVar))
  
#}