

# IS = importanceSampler
sampleFromIS <- function(noMuSamples = 1, selfNormalized = FALSE){
  # proposal dist: U[0,1]
  # density of proposal dist:
  g <- function(x) 1
  
  # target dist: B(a=10, b=3)
  a <- 10
  b <- 3
  # density of target dist
  f <- function(x) dbeta(x, a, b)
  
  # weight function w(x):
  weight <- function(x){
    # f(x) / g(x)
    ret <- f(x) / g(x)
  }


  # aproximate expectation of phi under f 
  approxExp <- function(phi, noOfSamples = 1000){
    # sample from uniform distribution
    samples <- runif(noOfSamples)
  
    weights <- weight(samples)
    # \frac{ \sum w_i(X_i) \phi(X_i) }{n}
    if(!selfNormalized){
      estMu <- sum(weights * phi(samples)) / noOfSamples
    }
    else{
      estMu <- sum(weights * phi(samples)) / sum(weights)
    }
    estMu
  }
  
  # sample noSamples of mu and variance
  muSamples <- replicate(noMuSamples, approxExp(identity))
  varSamples <- sapply(muSamples, function(estimatedMu){
    approxExp(function(x) (x - estimatedMu)^2)
  })
  list(muSamples, varSamples)
}


