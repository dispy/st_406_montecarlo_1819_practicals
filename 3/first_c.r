
# sample for 1,000 times and then reject samples according to rejection sampling
n <- 3600

# rejection sampler
sampleFromRS <- function(noMuSamples = 1){
  
  # target dist: B(a=10, b=3)
  a <- 10
  b <- 3
  # density of target dist
  f <- function(x) dbeta(x, a, b)
  
  
  # approximate expectation of phi under f 
  approxExp <- function(phi, noOfSamples = 3600){
    # simply the maximum of the density
    normalizingConstant <- 3.585
    
    x_samples <- runif(noOfSamples)
    height_samples  <- runif(noOfSamples)
    
    # take all x where U > dbeta(X, 10, 3) / normalizingConstant
    X <- x_samples[height_samples <= f(x_samples) / normalizingConstant]
    
    # estimate mean and variance
    estimatedMean <- sum(X) / length(X)
    estimatedMean
  }
  
  # sample noSamples of mu and variance
  muSamples <- replicate(noMuSamples, approxExp(identity))
  varSamples <- sapply(muSamples, function(estimatedMu){
    approxExp(function(x) (x - estimatedMu)^2)
  })
  list(muSamples, varSamples)
}


